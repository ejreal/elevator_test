<?php


class ElevatorController
{
     public $id;
     public $name;
     public $currentFloor = 0;
     public $currentDirection;
     public $travelDelay = 1;
     public $travelQueue = array();
     public $elevatorStarted = false;
     public $elevatorCanMove = true;

     public function __construct($key){
         $this->id = $key;
         $this->name = "Elevator $key";
     }

     public function getId(){
         return $this->id;
     }

     public function addFloorToTravelQueue($floor){
         array_push($this->travelQueue, $floor);
     }

     public function start(){
         //START ELEVATOR
         $this->elevatorStarted = true;
         sleep(1);
         $this->move($this);
     }

     public function move($elevator){
        if(!empty($elevator->travelQueue) && $elevator->elevatorCanMove){
            $elevator->elevatorCanMove = false;
            //echo "ELEVATOR: $elevator->id Moving from $elevator->currentFloor .";
            $elevator->currentDirection = $elevator->travelQueue[0] < $elevator->currentFloor ? "down": "up";
            //Sleep to make sure elevator car arrived to floor in the elevator $travelQueue
            sleep($elevator->travelDelay);
            $elevator->currentFloor = array_shift($elevator->travelQueue);
            //echo "ELEVATOR: $elevator->id Moved to $elevator->currentFloor .";
            $elevator->elevatorCanMove = true;
        }

     }

     public function findClosestElevator($elevators, $floor, $direction){
         $differenceTime = array();

         for ($x=0; $x < count($elevators); $x+=1){
             if($elevators[$x]->currentDirection === $direction || is_null($elevators[$x]->currentDirection ) && $elevators[$x]->elevatorCanMove){
                 $time = ($elevators[$x]->currentFloor - $floor) * $elevators[$x]->travelDelay;
                 $time = $time < 0 ? -$time : $time;
                 array_push($differenceTime, array("time" => $time, "elevator_id" => $elevators[$x]->id, "current_floor" => $elevators[$x]->currentFloor));
             }
         }
         //Sort Elevator ARRAY by time
         $minTime = array_column($differenceTime, 'time');
         array_multisort($minTime, SORT_ASC, $differenceTime);

         //Return the first elevator_id available;
         return $differenceTime[0]['elevator_id'];

     }



}