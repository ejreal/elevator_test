<?php

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
$dotenv->load();

class BuildingController
{
    public $floors;
    public $elevators = array();


    public function __construct(){
        $this->floors =$_ENV['FLOOR_COUNT'];
    }

    public function addElevator($elevator){
        array_push($this->elevators, $elevator);
    }

    public function requestElevator($floor, $direction){

        if($floor == $this->floors && $direction == "up"){
            return "Elevator only can go down from floor $floor.";
        }elseif ($floor == 1 && $direction == "down") {
            return "Elevator only can go up from floor $floor.";
        }else{
            $closestElevatorId = ElevatorController::findClosestElevator($this->elevators, $floor, $direction);
            for($x = 0; $x < count($this->elevators); $x+=1) {
                if($this->elevators[$x]->getId() === $closestElevatorId){
                    $this->elevators[$x]->addFloorToTravelQueue($floor);
                    $this->elevators[$x]->start();
                }
            }
            return "ELEVATOR: $closestElevatorId on it way $direction";
        }
    }

}