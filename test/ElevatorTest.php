<?php
    use PHPUnit\Framework\TestCase;
    class ElevatorTest extends  TestCase
    {
        public function testElevatorGet(){
            $this-> assertTrue(true);
        }

        public function testAddElevatorToBuilding(){
            //CREATE BUILDING
            $building = new BuildingController();

            //ADD ELEVATOR TO BUILDING
            $building->addElevator(new ElevatorController(0));
            $this->assertCount(1, $building->elevators);

        }

        public function testElevatorRequest(){
            $elevator_car_count = 3;
            //CREATE BUILDING
            $building = new BuildingController();
            //ADD multiple ELEVATORS TO BUILDING
            for($x = 0; $x < $elevator_car_count; $x+=1){
                $building->addElevator(new ElevatorController($x));
            }
            //Request elevator to go up from floor 2
            $requestFrom2FloorUp = $building->requestElevator(2, 'up');
            $this->assertEquals("ELEVATOR: 0 on it way up", $requestFrom2FloorUp);
            //Request elevator to down up from floor 5
            $requestFrom5FloorDown = $building->requestElevator(5, 'down');
            $this->assertEquals("ELEVATOR: 1 on it way down", $requestFrom5FloorDown);
            //Request elevator to go up from floor 6
            $requestFrom6FloorUp = $building->requestElevator(6, 'up');
            $this->assertEquals("Elevator only can go down from the 6.", $requestFrom6FloorUp);
        }

        public function testFirstFloorNewRequest(){
            $elevator_car_count = 3;
            //CREATE BUILDING
            $building = new BuildingController();
            //First Elevator Car
            $firstElevatorCar = new ElevatorController(0);
            $firstElevatorCar->currentFloor = 3;
            $building->addElevator($firstElevatorCar);
            //Second Elevator Car
            $secondElevatorCar = new ElevatorController(1);
            $secondElevatorCar->currentFloor = 5;
            $building->addElevator($secondElevatorCar);
            //Third Elevator Car
            $thirdElevatorCar = new ElevatorController(2);
            $thirdElevatorCar->currentFloor = 6;
            $building->addElevator($thirdElevatorCar);

            $requestFrom1FloorUp = $building->requestElevator(1, 'up');

            $this->assertEquals("ELEVATOR: 0 on it way up", $requestFrom1FloorUp);

        }

    }