<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';



$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
$dotenv->load();

//CREATE BUILDING
$building = new BuildingController();

//ADD ELEVATORS TO BUILDING
for($x = 0; $x < $_ENV['ELEVATOR_CAR_COUNT']; $x+=1){
    $building->addElevator(new ElevatorController($x));
}
//INITIAL CALL REQUESTS
$building->requestElevator(3, 'up');
$building->requestElevator(5, 'down');
$building->requestElevator(6, 'down');
$building->requestElevator(1, 'up');



$app = new \Slim\App();

$app->get('/request-elevator/{floor}/{direction}', function (Request $request, Response $response, $args) {
     global $building;
     $floor = (int)$args['floor'];
     $direction = $args['direction'];
     return $response->write($building->requestElevator($floor, $direction));
});

$app->run();